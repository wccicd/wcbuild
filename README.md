# What is this?
This is one repository of a group of repositories that helps with the build and deployment of customizations for HCL Commerce v9.

This is the *wcbuild* repository, responsible for creating a docker image of the WCBD utility to be used by other containers.

It is written as a Jenkins defined pipeline script and is intended to be used as is.

The "*wcbuild*" git repository (aka. this repository), is a collection of folder and scripts to help organize the utilities required to build the code. The assets will the packaged in to a container, the *WCBUILD* container. The *WCBUILD* container is then used by a specific git repository to build, package, and copy the necessary assets into the corresponding container: CRS, XC, TS, SEARCH, UTIL, to then create a container with assets already deployed.

# Features
The scripts have the following features:

  - Git auto tagging, uses GIT `TAGS` to denote the code that was used for construction the docker image.
  - Build bundles, creates the bundles for each component of commerce: crs, xc, ts, search, util
  - Docker Build, and tagging, makes use of the same GIT `TAG` to tag the docker image.

## How do I use it?
1. Install Jenkins in a VM 
1. Install/Use a GIT server of preference
1. ***In GIT:*** Override the content of the **wcbuild** git repo `wcbd/*` with the latest content of the WCB from the HCL Commerce utility image that you are using.
1. ***In Jenkins:*** create a multi-pipeline job for each individual GIT repository and point it to the **Jenkinsfile** file.
1. Connect the HCL Commerce toolkit to the GIT repository for **crs**
1. Make a change to a file
1. Commit and push the file to the **remote**
1. Wait for the pipeline to build the code, and push the change to the helm

## Where do the logs go:
As it is all enabled with Jenkins descriptive files, the logs can be found in the Jenkins console at each respective job.

# Appendix:

The script uses **Environment variables** as inputs needed for the build process.

```
   BUILD_IMAGE = 'wc-build'
   BUILD_IMAGE_TAG = 'latest'
   BUILD_TYPE = ''
   BUNDLE_DIR = '../../bundle-zips'
   CUSTCODE = 'dem'
   DOCKER_NAMESPACE = 'commerce'
   DOCKER_PASSWORD = ''
   DOCKER_REG = 'mycluster.icp:8500'
   DOCKER_USER = 'admin'
   DOSFTP = 'false'
   DROP_LOC = '/tmp'
   GIT_DOTAGS = 'true'
   GITOPS_PUSH = 'true'
   IMAGE_TYPE = 'crs-app'
   SLACK_URL="false"
   WC9_VERSION = '9.0.1.5'
   WCBD_BUNDLE_NAME = 'wcbd_files.zip'
   GIT_HELM_REPO_EMAIL = 'wccicd@test.com'
   GIT_HELM_REPO_USERNAME = 'wccicd'
   GIT_HELM_REPO_PW = 'passw0rd'
   GIT_HELM_REPO_BASE_DIR = 'helmcharts'
   GIT_HELM_REPO_URL = 'ssh://git@testgit.localdomain:30022/wccicd/helmcharts.git'
   AUTH_DIR = 'auth'
   LIVE_DIR = 'live'
   ICP_USER = 'admin'
   ICP_PW = 'admin'
   ICP_HOST = 'mycluster.icp'
```
