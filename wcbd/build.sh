#!/bin/bash

set -e
set -u

cd ../wcbd

export BUILD_TYPE="${2}"
export BUILD_NUMBER="${3}"
export DROP_LOC="${4}"
export CC="${1}-${BUILD_TYPE}" # CC is the customer code
export BUILD_LABEL=$CC-$BUILD_NUMBER

echo "############## Starting Build for $BUILD_LABEL of type $BUILD_TYPE from the following commit #########"

echo "############################## Directory Renaming #############################"
# sed -i s/@@BUILD_NUMBER@@/$BUILD_LABEL/g ../src/workspace/DataLoad/sql/common/appconfigparams.sql || true
if [ "${BUILD_TYPE}" = "crs" ] ; then
	cp -r ../src/crs/workspace/crs-web-delta/{src,WebContent} ../src/crs/workspace/crs-web || true
	mv ../src/crs/workspace/crs-web/src/main/resources/* ../src/crs/workspace/crs-web/src/ || true
	mv ../src/crs/workspace/crs-web-delta ../src/crs/workspace/crs-web-delta-old || true
elif [ "${BUILD_TYPE}" = "search" ] ; then
	mv -v ../src/search/workspace/search-ear-delta/lib/*.jar ../src/search/workspace/search-ear-delta
fi
for old in ../src/${BUILD_TYPE}/workspace/*-delta
do
	new=$(echo ${old} | sed s#-delta##)
	mv -v $old $new || true
done

mkdir -p dist/server
echo "############################## Starting Build #################################"
bash wcbd-ant -file wcbd-build.xml -Dapp.type=${BUILD_TYPE} -Dbuild.label=$BUILD_LABEL -Dwork.dir .

# Move File to location
mv ./dist/server/*.zip ${DROP_LOC}
