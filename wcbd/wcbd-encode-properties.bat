@echo off

REM -----------------------------------------------------------------
REM  Licensed Materials - Property of IBM
REM
REM  WebSphere Commerce
REM
REM  (C) Copyright IBM Corp. 2010 All Rights Reserved.
REM
REM  US Government Users Restricted Rights - Use, duplication or
REM  disclosure restricted by GSA ADP Schedule Contract with
REM  IBM Corp.
REM -----------------------------------------------------------------

REM ----------------------------------------------------------------------------
REM This batch file invokes Ant to encode a properties file.
REM ----------------------------------------------------------------------------

setlocal

if not exist setenv.bat (
    echo ERROR: setenv.bat does not exist.
    goto END_ERR
)

call setenv.bat

if x%ANT_HOME%x==xx (
    echo ERROR: ANT_HOME is not set in setenv.bat.
    goto END_ERR
)
if x%WAS_HOME%x==xx (
    echo ERROR: WAS_HOME is not set in setenv.bat.
    goto END_ERR
)

call %ANT_HOME%\bin\ant.bat -buildfile wcbd-encode-properties.xml -Dwas.home="%WAS_HOME%" %*
if %ERRORLEVEL% neq 0 goto END_ERR
goto END

:END_ERR
endlocal
if "%WCBD_DIRECT_CALL%"=="Y" (
    exit 1
) else (
    exit /b 1
)

:END
endlocal
